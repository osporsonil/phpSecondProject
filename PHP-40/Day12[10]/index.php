<?php

include_once("vendor/autoload.php");
use \Bitm\Car\Car;
use \Bitm\Car\Bmw;
use \Bitm\Car\Toyota;


$myBmw = new Bmw("Green","Dhaka-KA-1235",33);
$myBmw->fill(10)
      ->fill(30)
      ->fill(5);

echo $myBmw->calculateApproximateDistance();
$friendCar = new Toyota("Red","Dhaka-JA-2365",33);

//$friendBmw2 = new Car();
echo $myBmw->fill(50)
           ->ride(10)
           ->calculateApproximateDistance();
$myBmw->setModel("BMW 1 Series");

?>

<html>
<body>
<table border="1">
    <tr>
        <th>Rented By</th>
        <th>Color</th>
        <th>Licence</th>
        <th>Details</th>

    </tr>
    <tr>
        <td>Me</td>
        <td><?php echo $myBmw->color; ?></td>
        <td><?php echo $myBmw->licence; ?></td>
        <td><?php echo $myBmw; ?></td>
    </tr>
    <tr>
        <td>Friend</td>
        <td><?php echo $friendCar->color; ?></td>
        <td><?php echo $friendCar->licence; ?></td>
        <td><?php echo $friendCar; ?></td>
    </tr>

</table>
</body>
</html>
