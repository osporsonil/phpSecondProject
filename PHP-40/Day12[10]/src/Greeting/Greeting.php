<?php
namespace Bitm\Greeting;


Class Greeting{

    public $message = "";

    public function __construct($message="Hello World"){
        $this->message = $message;
    }

    public function sayGreeting(){
        return $this->message;
    }

}