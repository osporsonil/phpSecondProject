<?php

namespace Bitm\Car ;

class Car
{   public $make='';
    public $color='';
    public $licence='';
    protected $model='';
    public $millage=1;
    public $tank=0.0;
    public $approxDistance=0;

    public function __construct(String $color="",$licence="",int $millage)
    {
        $this->color = $color;
        $this->licence = $licence;
        $this->millage = $millage;

    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return "Licence of This Car is: ".$this->licence
        ."<br>Millage of This Car is: ".$this->millage
        ."<br> Current fuel of This Car is: ". $this->tank . " Gallon"
        ."<br> This Car Can Run Approximately : " .$this->calculateApproximateDistance()." Miles"
        ."<br> Make of this Car Is : " .$this->make ."<br> Make of this Car Is : " .$this->model;
    }

    public function fill(float $fuel){
        // $this->tank =$this->tank+ $fuel;
        $this->tank += $fuel;
        return $this;

    }
    public function calculateApproximateDistance(){
        $this->approxDistance = $this->tank*$this->millage;
        return $this->approxDistance;
    }

    public function ride(int $distance=0)
    {
        $this->tank -= $distance/$this->millage;
        return $this;

    }

   final public function setModel($model)
    {
        $this->model=$model;
    }
}



