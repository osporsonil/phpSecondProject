<?php

namespace Bitm\Car ;

class Bmw extends Car
{

    public function __construct(String $color="",$licence="",int $millage)
    {
        parent::__construct($color,$licence,$millage);
        $this->make=__CLASS__;
    }

    public function setModel($model)
    {
        $this->model="Override ".$model;
    }

}



