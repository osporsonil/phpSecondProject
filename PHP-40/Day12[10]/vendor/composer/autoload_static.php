<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit01ebc51aa52814d4315dfdbf43969bcc
{
    public static $prefixLengthsPsr4 = array (
        'F' => 
        array (
            'Faker\\' => 6,
        ),
        'B' => 
        array (
            'Bitm\\' => 5,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Faker\\' => 
        array (
            0 => __DIR__ . '/..' . '/fzaninotto/faker/src/Faker',
        ),
        'Bitm\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit01ebc51aa52814d4315dfdbf43969bcc::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit01ebc51aa52814d4315dfdbf43969bcc::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
