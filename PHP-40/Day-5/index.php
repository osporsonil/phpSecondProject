<?php

if (isset($_POST['number1']) && isset($_POST['number2']) && isset($_POST['operator'])) {
    $number1 = $_POST['number1'];
    $number2 = $_POST['number2'];
    $operator = $_POST['operator'];

    if ($operator == 'add') {
        echo $number1 + $number2;
    } elseif ($operator == 'sub') {
        echo $number1 - $number2;
    } elseif ($operator == 'multi') {
        echo $number1 * $number2;
    } elseif ($operator == 'div') {
        echo $number1 / $number2;
    }

}

?>
<html>
<head>
    <meta charset="utf-8">
    <title>Calculator PHP</title>
</head>
<body>
<h1>Calculator</h1>

<form action="action.php" method="post">
    <label for="">Number 1</label> <br>
    <input type="number" name="number1"> <br>
    <label for="">Number 2</label> <br>
    <input type="number" name="number2"> <br>

    <label for="">Add
        <input type="radio" name="operator" value="add">
    </label>
    <label for="">Sub
        <input type="radio" name="operator" value="Sub">
    </label>
    <label for="">Multi
        <input type="radio" name="operator" value="multi">
    </label>
    <label for="">divi
        <input type="radio" name="operator" value="div">
    </label>

    <button type="submit">Calculate</button>
    <br>
</form>
</body>
</html>