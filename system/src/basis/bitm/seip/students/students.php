<?php 
	
	namespace app\basis\bitm\seip\students;

	use PDO;

	class students{

		public $name = "";
		public $email = "";
		public $institute = "";
		public $id = "";


		public function setData($data = ""){

			if (array_key_exists('name', $data)) {
				$this->name = $data['name'];
				
			}

			if (array_key_exists('institute', $data)) {
				$this->institute = $data['institute'];
			}

			if (array_key_exists('email', $data)) {
				$this->email = $data['email'];
			}

			if (array_key_exists('id', $data)) {
				$this->id = $data['id'];
			}
			
			return $this;

		}


		public function index(){

			try {
				  $pdo = new PDO('mysql:host=localhost;dbname=boys', "root", "");
				 
				  $query = "SELECT * FROM `boys`";
				  $stmt = $pdo->prepare($query);
				  $stmt->execute();
				  $data = $stmt->fetchAll();

				  return $data;
				 
				  
				} catch(PDOException $e) {
				  echo 'Error: ' . $e->getMessage();
				}

		}

		public function search(){
			try {
				  $pdo = new PDO('mysql:host=localhost;dbname=boys', "root", "");
				 
				  $query = "SELECT * FROM `boys` where name = :name or institute = :institute or email = :email";
				  $stmt = $pdo->prepare($query);
				  $stmt->execute(
				  		array(
						    ':name' => $this->name,
						  )
				  	);
				  $data = $stmt->fetchAll();

				
				// if ($stmt) {
				//  	session_start();
				// 	$_SESSION['massage'] = "The search result Match";
				//  }
				 return $data;
				  
				} catch(PDOException $e) {
				  echo 'Error: ' . $e->getMessage();
				}

		}

		public function show(){
			try {
				  $pdo = new PDO('mysql:host=localhost;dbname=boys', "root", "");
				 
				  $query = "SELECT * FROM `boys` where id=$this->id";
				  $stmt = $pdo->prepare($query);
				  $stmt->execute();
				  $data = $stmt->fetch();

				  return $data;
				 
				  
				} catch(PDOException $e) {
				  echo 'Error: ' . $e->getMessage();
				}
		}


		public function update(){
			try {
				  $pdo = new PDO('mysql:host=localhost;dbname=boys', "root", "");
				 
				  $query = "UPDATE boys SET name=:name, institute=:institute, email = :email WHERE id=:id";
				  $stmt = $pdo->prepare($query);
				  $stmt->execute(array(
				    ':id' => $this->id,
				    ':name' => $this->name,
				    ':institute' => $this->institute,
				    ':email' => $this->email
				  ));
				 
				if ($stmt) {
					
					session_start();
					$_SESSION['massage'] = "Successfully updated";

					header("location:index.php");
				}

				  
				} catch(PDOException $e) {
				  echo 'Error: ' . $e->getMessage();
		}






	}




		public function delete(){
			try {
				  $pdo = new PDO('mysql:host=localhost;dbname=boys', "root", "");
				 
				  $query =  "DELETE FROM `boys` WHERE `boys`.`id` = $this->id";
				  $stmt = $pdo->prepare($query);
				  $stmt->execute();
				 
				 if ($stmt) {
				 	session_start();
					$_SESSION['massage'] = "Successfully Deleted From Database";
				 	header("location:index.php");

				 }
				  
				} catch(PDOException $e) {
				  echo 'Error: ' . $e->getMessage();
				}
		}


		public function store(){
			try {
				  $pdo = new PDO('mysql:host=localhost;dbname=boys', "root", "");
				 
				  $query = "INSERT INTO `boys` (`id`, `name`, `institute`, `email`) VALUES (:id, :name, :institute, :email)";
				  $stmt = $pdo->prepare($query);
				  $stmt->execute(array(
				    ':id' => null,
				    ':name' => $this->name,
				    ':institute' => $this->institute,
				    ':email' => $this->email
				  ));
				 
				if ($stmt) {
					
					session_start();
					$_SESSION['massage'] = "Successfully Submitted";

					header("location:create.php");
				}

				  
				} catch(PDOException $e) {
				  echo 'Error: ' . $e->getMessage();
		}





	}

}




 ?>