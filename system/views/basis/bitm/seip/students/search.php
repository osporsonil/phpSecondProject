<?php 

include_once("../../../../../vendor/autoload.php");

use app\basis\bitm\seip\students\students;



$obj = new students();

$values = $obj->setData($_GET)->search();

if (isset($_SESSION['massage'])) {
	echo $_SESSION['massage'];
	unset($_SESSION['massage']);
}



 ?> 



 <!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Student List</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="section-main-area">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="view">
					<table class="table table-striped">
					    <thead>
					      <tr class="success">
					        <th>ID</th>
					        <th>Name</th>
					        <th>Institute</th>
					        <th>E-mail</th>
					        <th>Action</th>
					      </tr>
					    </thead>
					    <tbody>
					    <?php 
					    $id = 1;
					    foreach($values as $value):  ?>
					      <tr>
					        <td><?php echo $id++; ?></td>
					        <td><?php echo $value['name']; ?></td>
					        <td><?php echo $value['institute']; ?></td>
					        <td><?php echo $value['email']; ?></td>
					        <td><a class="btn btn-info" href="show.php?id=<?php echo $value['id']; ?>">View</a> <a class="btn btn-success" href="edit.php?id=<?php echo $value['id'];?>">Edit</a> <a class="btn btn-danger" href="delete.php?id=<?php echo $value['id']; ?>">Delete</a></td>
					      </tr>
					     <?php endforeach; ?>
					    </tbody>
					  </table>
				</div>
			</div>
		</div>
	</div>
</div>


</body>
</html>