<?php 
	session_start();
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PHP MySQL</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<header style="background: #A0D169; color:#FFF; margin-bottom: 30px" class="header-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="text-center title">
						<h1>Add Students Informations</h1>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section class="main-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-2">
					<h2>Watch List ?</h2>
					<a class="btn btn-info" href="index.php">Click Here</a>
				</div>
				<div class="col-sm-8">
					<div class="main">
						<form action="store.php" method="POST">
						  <div class="form-group">
						    <label for="name">Your Name:</label>
						    <input type="text" name="name" class="form-control" id="name">
						  </div>
						  <div class="form-group">
						    <label for="ins">Institute:</label>
						    <input type="text" name="institute" class="form-control" id="ins">
						  </div>
						  <div class="form-group">
						    <label for="email">Email address:</label>
						    <input type="email" name="email" class="form-control" id="email">
						  </div>

						  <button type="submit" class="btn btn-success">Send</button>
						</form>
						<p>
						<?php if (isset($_SESSION['massage'])) {
										echo $_SESSION['massage'];
										unset($_SESSION['massage']);
									} ?>
						</p>
					</div>
				</div>
				<div class="col-sm-2">

				</div>
			</div>
		</div>
	</section>

	
</body>
</html>