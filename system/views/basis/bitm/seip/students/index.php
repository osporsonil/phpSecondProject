<?php 

include_once("../../../../../vendor/autoload.php");

use app\basis\bitm\seip\students\students;

session_start();

$obj = new students();

$values = $obj->index();

if (isset($_SESSION['massage'])) {
	echo $_SESSION['massage'];
	unset($_SESSION['massage']);
} 

 ?> 

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Student List</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

	<header style="background: #000000;color:#FFF;margin-bottom:30px" class="header-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="text-center title">
						<h1 style="margin:0;padding:15px 0">Students List</h1>
					</div>
				</div>
			</div>
		</div>
	</header>

	<section class="student-list-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="student-list">
						
						<table class="table table-striped">
						    <thead>
						      <tr class="success">
						        <th>ID</th>
						        <th>Name</th>
						        <th>Institute</th>
						        <th>E-mail</th>
						        <th>Action</th>
						      </tr>
						    </thead>
						    <tbody>
						    <?php 
						    $id = 1;
						    foreach($values as $value):  ?>
						      <tr>
						        <td><?php echo $id++; ?></td>
						        <td><?php echo $value['name']; ?></td>
						        <td><?php echo $value['institute']; ?></td>
						        <td><?php echo $value['email']; ?></td>
						        <td><a class="btn btn-info" href="show.php?id=<?php echo $value['id']; ?>">View</a> <a class="btn btn-success" href="edit.php?id=<?php echo $value['id'];?>">Edit</a> <a class="btn btn-danger" href="delete.php?id=<?php echo $value['id']; ?>">Delete</a></td>
						      </tr>
						     <?php endforeach; ?>
						    </tbody>
						  </table>
						  <span>Download as<a href="pdf.php">PDF</a>||<a href="xl.php">Excel</a> || <a class="btn btn-success" href="create.php">Create New</a></span>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="search-area">
						<form action="search.php" method="GET">
							<div class="form-group">
								<label for="name">Search:</label>
								<input type="text" name="name" class="form-control" id="name" placeholder="Search Here"/>
							</div>
							<button type="submit" class="btn btn-success">Search</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>

	
</body>
</html>