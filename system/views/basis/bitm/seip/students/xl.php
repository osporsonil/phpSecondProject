<?php 

	error_reporting(E_ALL);
	error_reporting(E_ALL & ~E_DEPRECATED);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);

	data_default_timezone_set('Europe/London');


	if (PHP_SAPI == 'cli'):
		die('This example should only be run from a web Browser');



	// include php excel

	require_once("../../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php");
	include_once("../../../../../vendor/autoload.php");

	use app\basis\bitm\seip\students\students;


	$obj = new students();
	$allData = $obj->index();

	// create new PHPExcel

	$objPHPExcel = new PHPExcel();

	// set document properties

	$objPHPExcel->getProperties()->setCreator("Martin Balliauw")
	 ->setLastModifiedBy("Martin Balliauw")
	 ->setTitle("Office 2007 XLSX Test Document")
	 ->setSubject("Office 2007 XLSX Test Document")
	 ->setDescription("Test Document for Office 2007 XLSX, generated using PHP calsses")
	 ->setKeywords("Office 2007 openxml php")
	 ->setCategory("Test result file");

	 // add some data

	 $objPHPExcel->setActiveSheetIndex(0)
	  ->setCellValue('A1', 'SL')
	  ->setCellValue('B1', 'ID')
	  ->setCellValue('C1', 'Book Title');

	  $counter = 2;
	  $serial = 0;



	 foreach ($allData as $data){
	 	$serial++;
	 	$objPHPExcel->setActiveSheetIndex(0)
	 	 ->setCellValue('A'.$counter , $serial)
	 	 ->setCellValue('B'.$counter , $data['name'])
	 	 ->setCellValue('C'.$counter , $data['institute'])
	 	 ->setCellValue('D'.$counter , $data['email']);
	 	 $counter++;
	 }

	 // Rename WorkSheet
	 $bojPHPExcel->getActiveSheet()->setTitle('Mobile_List');


	 // set active sheet index to the first sheet, so excel opens this as the first sheet 

	 $objPHPExcel->setActiveSheetIndex(0);

	 // Redirece output to the client browser(Excel15)

	 header("Content-Type: application/vnd.ms-excel");
	 header("Content-Desposition: attachment:filename='01simple.xls'");
	 header('Cache-Control: max-age=0');

	 // if you serving IE9 then the following may be needed

	 header('Cache-Control: max-age=0');

	 // if you are serving to the IE over SSL, then the following may be needed 

	 header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); //date in the past 
	 header('Last-Modified: '. gmdate('D, d M Y H:i:s'.'GMT'));
	 header('Cache-Control: cache, must revalidate');
	 header('Pragma: public');

	 $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5')
	 $objWriter->save('php://output');
	 exit;








 ?>