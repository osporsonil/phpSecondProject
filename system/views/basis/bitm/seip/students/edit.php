<?php 

include_once("../../../../../vendor/autoload.php");

use app\basis\bitm\seip\students\students;

$obj = new students();

$details = $obj -> setData($_GET)->show();

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Update</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<header style="background: #A0D169; color:#FFF; margin-bottom: 30px" class="header-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="text-center title">
						<h1>Update Your Informations</h1>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section class="main-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-2">
					<!-- <h2>Watch List ?</h2>
					<a class="btn btn-info" href="index.php">Click Here</a> -->
				</div>
				<div class="col-sm-8">
					<div class="main">
						<form action="update.php" method="POST">
						  <div class="form-group">
						    <label for="name">Your Name:</label>
						    <input type="text" name="name" value="<?php echo $details['name']; ?>" class="form-control" id="name">
						  </div>
						  <div class="form-group">
						    <label for="ins">Institute:</label>
						    <input type="text" name="institute" value="<?php echo $details['institute']; ?>" class="form-control" id="ins">
						  </div>
						  <div class="form-group">
						    <label for="email">Email address:</label>
						    <input type="email" name="email" value="<?php echo $details['email']; ?>" class="form-control" id="email">
						  </div>
						  	<input type="hidden" value="<?php echo $details['id']; ?>" name="id" class="form-control" id="id">
						  
							<button type="submit" class="btn btn-success">UPDATE</button>
						</form>
						<p>
						</p>
					</div>
				</div>
				<div class="col-sm-2">

				</div>
			</div>
		</div>
	</section>

	
</body>
</html>