<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit87020301d987fd79d3780231f2e84392
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit87020301d987fd79d3780231f2e84392::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit87020301d987fd79d3780231f2e84392::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
