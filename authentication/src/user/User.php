<?php

class User
{
    public $username = '';
    public $email = '';
    public $pdo = '';
    public $password = '';

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=php38', 'root', '');

    }

    public function setData($data = '')
    {

            if(array_key_exists('username', $data))
            {
            $this->username =$data['username'];
            }
            if(array_key_exists('email', $data))
            {
                $this->email =$data['email'];
            }
            if(array_key_exists('password', $data))
            {
                $this->password =$data['password'];
            }

//        echo "<pre>";
//        var_dump($this);
//
//        die();

            return $this;
    }
    public function store()
    {
        try {
            $query = "INSERT INTO `users` (`id`,`username`, `email`, `password`, `token`) VALUES (:id, :uname, :email, :pw, :tk)";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => null,
                    ':uname' => $this->username,
                    ':email' => $this->email,
                    ':pw'=> $this->password,
                    ':tk'=>'asdf',
                )
                    );
            if ($stmt) {

                $_SESSION['message'] = "succesfully Registard";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public  function userAvailability() {
        try {
            $query = "SELECT * FROM `users` WHERE username= '$this->username' or email= '$this->email'";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public  function login()
    {
        try {
            $query = "SELECT * FROM `users` WHERE (username= '$this->username' or email= '$this->email' )AND password= '$this->password'";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }
    public function index()
    {
        try {
            $query = "SELECT * FROM `users` WHERE deleted_at='0000-00-00 00:00:00 '";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function search()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=php38', "root", "");

            $query = "SELECT * FROM `users` where id = :id ";
            $stmt = $pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                )
            );
            $data = $stmt->fetchAll();


            return $data;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }




    public function show()
    {
        try {
            $query = "SELECT * FROM `users` WHERE unique_id='$this->id'";

            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();

            if (empty($data)) {
                $_SESSION['message'] = "something wrong";
                header('location:index.php');
            } else {
                return $data;
            }


        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function update()
    {


        try {
            $query = "UPDATE users SET title=:title WHERE id=:id";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                    ':title' => $this->title
                )
            );
            if ($stmt) {

                $_SESSION['message'] = "succesfully updated";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function trash()
    {
        try {
            $query = "UPDATE users SET deleted_at=:datetme WHERE id=:id";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                    ':datetme' => date('y-m-d h:m:s'),
                )
            );
            if ($stmt) {

                $_SESSION['message'] = "succesfully deleted ";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function trashlist()
    {
        try {
            $query = "SELECT * FROM `users` WHERE deleted_at !='0000-00-00 00:00:00 '";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function restore()
    {
        try {
            $query = "UPDATE users SET deleted_at=:datetme WHERE id=:id";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                    ':datetme' => '0000-00-00 00:00:00',
                )
            );
            if ($stmt) {

                $_SESSION['message'] = "succesfully Restored ";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function delete()
    {
        try {
            $query = "DELETE FROM `users` WHERE `students`.`id` = $this->id";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            if ($stmt) {

                $_SESSION['message'] = "<h1>Succesfully Deleted</h1>";
                header('location:trashlist.php');
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}