<?php

include_once("../../../../../vendor/autoload.php");
use App\module1\BITM\SEIP\Students\Student;

$obj = new Student();
$alldata = $obj->trashlist();


if(isset($_SESSION['message']))
{
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
?>

<html>
<head>
    <title>List of student</title>
</head>


<body>
<a href="create.php">Add new</a>
<a href="trashlist.php">See deleted item</a>
<a href="index.php"> See original list</a>

<table border="1">
    <tr>
        <td>Id</td>
        <td>Title</td>
        <td>Action</td>
    </tr>
    <?php
    $id = 1;
    foreach ($alldata as $key => $value) {


        ?>

        <tr>
            <td><?php echo $id++ ?></td>
            <td><?php echo $value['title'] ?></td>
            <td>
                <a href="show.php?id=<?php echo $value['unique_id'] ?>">View Details</a> |
                <a href="edit.php?id=<?php echo $value['id'] ?>">Edit</a> |
                <a href="restore.php?id=<?php echo $value['id'] ?>">Restore</a> |

                <a href="delete.php?id=<?php echo $value['id'] ?>">Delete</a>
            </td>

        </tr>
    <?php } ?>
</table>
</body>

</html>