 <?php

include_once("../../../../../vendor/autoload.php");
use App\module1\BITM\SEIP\Students\Student;

$obj = new Student();
$alldata = $obj->index();


if(isset($_SESSION['message']))
{
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}
?>

<html>
<head>
    <title>List of student</title>
</head>


<body>
<a href="create.php">Add new</a>
<a href="trashlist.php">See deleted item</a>

<table border="1">
    <tr>
        <td>Id</td>
        <td>Title</td>
        <td>Action</td>
    </tr>

    <?php
    $id = 1;
    foreach ($alldata as $key => $value) {


        ?>

        <tr>
            <td><?php echo $id++ ?></td>
            <td><?php echo $value['title'] ?></td>
            <td>
                <a href="show.php?id=<?php echo $value['unique_id'] ?>">View Details</a> |
                <a href="edit.php?id=<?php echo $value['unique_id'] ?>">Edit</a> |
                <a href="trash.php?id=<?php echo $value['id'] ?>">Delete</a>
            </td>

        </tr>
    <?php } ?>

</table><br>
<span id="utility">download as<a href="pdf.php">PDF</a> | <a href="create.php">create new</a> </span>
<form action="search.php" method="post">
    <tr>
        <td><input type="search" name="search"> </td>
        <td><input type="button" value="search"> </td>

    </tr>

</form>
</body>

</html>