<?php
namespace App\module1\BITM\SEIP\Students;
use PDO;
class Student
{
    public $title = '';
    public $id ='';
    public $pdo='';
    public  function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=php38', 'root', '');

    }

    public function setData($data = '')
    {

        if(array_key_exists('title', $data)){
            $this->title = $data ['title'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data ['id'];
        }

        return $this;
    }

    public  function index()
    {
        try {
            $query = "SELECT * FROM `student_hobies` WHERE deleted_at='0000-00-00 00:00:00 '";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function search(){
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=php38', "root", "");

            $query = "SELECT * FROM `student_hobies` where id = :id ";
            $stmt = $pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                )
            );
            $data = $stmt->fetchAll();


            return $data;

        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }


    public function store()
    {
        try {
            $query = "INSERT INTO `student_hobies` (`id`,`unique_id`, `title`) VALUES (:a, :uid, :b)";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':a' => null,
                    ':uid'=>uniqid(),
                    ':b' => $this->title
                )
            );
            if($stmt){

                $_SESSION['message'] ="succesfully submited";
               header('location:create.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public  function  show()
    {
        try {
            $query = "SELECT * FROM `student_hobies` WHERE unique_id='$this->id'";

            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();

            if(empty($data))
            {
                $_SESSION['message']="something wrong";
                header('location:index.php');
            }
            else
            {
                 return $data;
            }




        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function update()
    {


        try {
            $query = "UPDATE student_hobies SET title=:title WHERE id=:id";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                    ':title' => $this->title
                )
            );
            if($stmt){

                $_SESSION['message'] ="succesfully updated";
               header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public  function trash()
    {
        try {
            $query = "UPDATE student_hobies SET deleted_at=:datetme WHERE id=:id";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                    ':datetme' => date('y-m-d h:m:s'),
                )
            );
            if($stmt){

                $_SESSION['message'] ="succesfully deleted ";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public  function trashlist()
    {
        try {
            $query = "SELECT * FROM `student_hobies` WHERE deleted_at !='0000-00-00 00:00:00 '";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public  function restore()
    {
        try {
            $query = "UPDATE student_hobies SET deleted_at=:datetme WHERE id=:id";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => $this->id,
                    ':datetme' => '0000-00-00 00:00:00',
                )
            );
            if($stmt){

                $_SESSION['message'] ="succesfully Restored ";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public  function delete()
    {
        try {
            $query = "DELETE FROM `student_hobies` WHERE `students`.`id` = $this->id";


            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            if($stmt){

                $_SESSION['message'] = "<h1>Succesfully Deleted</h1>";
                header('location:trashlist.php');
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}