<?php
namespace SP\Profile;
use PDO;
class Profile{
public $name,$password,$email,$gender,$id;


    public function setData($data=''){
        if(array_key_exists('ID',$data)){
            $this->id=$data['ID'];
        }
        if(array_key_exists('Name',$data)){
            $this->name=$data['Name'];
        }
        if(array_key_exists('Password',$data)){
            $this->password=$data['Password'];
        }
        if(array_key_exists('E-mail',$data)){
            $this->email=$data['E-mail'];
        }
        if(array_key_exists('Gender',$data)){
            $this->gender=$data['Gender'];
        }

        return $this;
    }
    public function store()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=profile_info','root','');
            $query = "INSERT INTO `pro_info` (`ID`, `Name`, `Password`, `E-mail`, `Gender`) VALUES (:a, :b, :c, :d, :e)";
            $stmt=$pdo->prepare($query);
            $stmt->execute(
                array(
                    ':a' => '',
                    ':b' => $this->name,
                    ':c' => $this->password,
                    ':d' => $this->email,
                    ':e' => $this->gender


        )
        );
            if($stmt){
                session_start();
                $_SESSION['msg']="Suceessfully updated";
                header('location:create.php');
            }
        }
        catch(PDOException $e){
            echo "ERROR: ".$e->getMessage();
        }
        }
    public function showall()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=profile_info','root','');
            $query = "Select * from pro_info";
            $stmt=$pdo->prepare($query);
            $stmt->execute();
            $db=$stmt->fetchAll();
            return $db;
        }
        catch(PDOException $e){
            echo "ERROR: ".$e->getMessage();
        }
    }
    public function show()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=profile_info','root','');
            $query = "Select * from pro_info WHERE id=:id";

echo $this->id;
            $stmt=$pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id'=>$this->id
                )
            );
            $db=$stmt->fetch();
            return $db;
        }
        catch(PDOException $e){
            echo "ERROR: ".$e->getMessage();
        }
    }
    public function delete()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=profile_info','root','');
            $query = "Delete from pro_info WHERE id=:id";


            $stmt=$pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id'=>$this->id
                )
            );
            $db=$stmt->fetch();
            $_SESSION['msg']="Sucessfully deleted";
            header('location:show.php');

        }
        catch(PDOException $e){
            echo "ERROR: ".$e->getMessage();
        }
    }
    public function update()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=profile_info','root','');
            $query = "Update `pro_info` set Name=:name,Passward=:password,E-mail=:email,Gender=:gender WHERE ID=:id";
            $stmt=$pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id'   => $this->id,
                    ':name' => $this->name,
                    ':password'=> $this->password,
                    ':email'=>$this->email,
                    ':gender'=>$this->gender
                )
            );
            if($stmt) {
                $_SESSION['msg'] = "updated successfully";
            }

        }
        catch(PDOException $e){
            echo "ERROR: ".$e->getMessage();
        }
    }
}